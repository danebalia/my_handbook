# Manager's User Manual

## What is this
It's the *essence* of how I operate as a Manager.  
This is not a list of rules of how I operate, but the under-current of values and thinking that guides those operations.

## I am human
I am a passionate husband and father. My family is a strong focus and my inspiration.
I am an introvert, a creative, a change agent, potential seeker and a servant-leader.
An avid reader and struggling gamer, yeah 3 kids ;)
Truly enjoy [yoga!](https://www.youtube.com/user/yogawithadriene)

**My approach to life is guided by 5 principals (5 BE's):**
1. Be Simple - Large doors swing on small hinges (problems)
2. Be Confident - Know you can. Trust you can. Practice so you can (attitude)
3. Be Focused - Limit the scope to whats manageable and what produces the most immediate feedback (productivity)
4. Be True - True To Self. True To Others. Seek truth in every situation as it's the only effective way to diagnose reality (character)
5. Be Consistent - Habits done consistently produce effectivity (actions)

## Three core values
1. [Radical Candour](https://www.radicalcandor.com/) - caring for others requires candid communication. 
2. [Mutual Purpose](https://www.amazon.com/Crucial-Conversations-Talking-Stakes-Second/dp/1469266822) - manage the stories we tell ourselves about others. In conflict seek to understand your purpose and that of the other person. 
3. [Psychological Safety](https://www.timetothink.com/book/time-to-think/) - the effectiveness of our thinking relates to how we treat one another

## How I manage
1. I aim to set clear expectations and be direct about it's accountability. When I fail to do so adequately, please feedback.
2. I have a natural and genuine care for those who I am responsible for.
3. I eagerly welcome feedback with facts, examples and/or specifics. Vice versa.
4. If it doesn't work, lets build a plan, or just get rid of it.
5. I aim to do whats right. But sometimes right is not effective. If not harmful, do whats effective.
6. Everyone desires to give their best and is sincere in what they are trying to communicate. 
7. "We don't click" can be an emotion based on the spiralling stories we have told ourselves. Seperate the facts from the stories.
8. I enjoy collaboration particularly with the multiplicity and diversity of minds.
9. If you not getting to deep work, reach out, I will do my utmost to figure out how to unblock you.
10. I am work in progress.

## Areas I'm working to improve...
1. Switching too quickly from problem definition to problem resolution.
2. Effecting change at a less rapid rate (at times).

